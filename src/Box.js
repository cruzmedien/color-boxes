import React, { Component } from "react";
import { choice } from "./helpers";
import "./Box.scss";

class Box extends Component {
  // =====================================
  constructor(props) {
    super(props);
    this.state = { color: choice(this.props.colors) };
    this.handleClick = this.handleClick.bind(this);
  }
  // =====================================
  pickColor() {
    let newColor;

    // Aqui realizamos el Loop mientras que newColor no sea igual que this.state.color. Asi no repetimos los colores.
    do {
      newColor = choice(this.props.colors);
    } while (newColor === this.state.color);

    this.setState({ color: newColor });
  }

  handleClick() {
    // Now we use the reeal function who going to make the action
    this.pickColor();
  }
  // =====================================
  render() {
    return (
      <div
        className="Box"
        style={{ backgroundColor: this.state.color }}
        onClick={this.handleClick}
      />
    );
  }
}

export default Box;
